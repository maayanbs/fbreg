// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: "AIzaSyCsTHqZxvYKjQWQO6Ae3yCrmVNhVzRRB9w",
    authDomain: "ex6ex6-5c3cf.firebaseapp.com",
    databaseURL: "https://ex6ex6-5c3cf.firebaseio.com",
    projectId: "ex6ex6-5c3cf",
    storageBucket: "ex6ex6-5c3cf.appspot.com",
    messagingSenderId: "865195949515"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
